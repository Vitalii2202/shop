# -*- coding: utf-8 -*-
from django.db import models
import random
from django.conf import settings
from mptt.models import MPTTModel, TreeForeignKey
from tinymce import models as tinymce_model


def make_upload_path(instance, filename, prefix = False):
    # Переопределение имени загружаемого файла.
    n1 = random.randint(0,10000)
    n2 = random.randint(0,10000)
    n3 = random.randint(0,10000)
    filename = str(n1)+"_"+str(n2)+"_"+str(n3) + '.jpg'
    return u"%s/%s" % (settings.IMAGE_UPLOAD_DIR, filename)

class Category(MPTTModel):
    name = models.CharField(max_length=150, default="", blank=True, verbose_name="Категория")
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    title = models.CharField(max_length=200, default="", verbose_name="Заголовок", blank=True)
    meta_desc = models.CharField(max_length=200,default="", verbose_name="Мета описание", blank=True)
    meta_key = models.CharField(max_length=200, default="", verbose_name="Ключевые слова", blank=True)
    slug = models.CharField(max_length=250, default="", blank=True, verbose_name="Урл")
    image = models.ImageField(upload_to=make_upload_path, default="",  blank=True,  verbose_name="Изображение")
    published = models.BooleanField(verbose_name=u"Опубликован")
    ordering = models.IntegerField(verbose_name=u"Порядок сортировки", default=0, blank=True, null=True)
    def __unicode__(self):
        return self.name
    
    def pic(self):
        if self.image:
            return u'<img src="%s" width="70"/>' % self.image.url
        else:
            return '(none)'
    pic.short_description = u'Изображение'
    pic.allow_tags = True
    
    class Meta:
        verbose_name_plural = "Категории"
        verbose_name = "Категория"
    
    class MPTTMeta:
        order_insertion_by = ['name']
        
        
class Produkt(models.Model):
    name = models.CharField(max_length=150, default="", blank=True, verbose_name="Название")
    category = models.ManyToManyField(Category,  related_name='cat')
    title = models.CharField(max_length=200, default="", verbose_name="Заголовок", blank=True)
    meta_desc = models.CharField(max_length=200,default="", verbose_name="Мета описание", blank=True)
    meta_key = models.CharField(max_length=200, default="", verbose_name="Ключевые слова", blank=True)
    slug = models.CharField(max_length=250, default="", blank=True, verbose_name="Урл")
    image = models.ImageField(upload_to=make_upload_path, default="",  blank=True,  verbose_name="Изображение")
    short_text = tinymce_model.HTMLField(blank=True, verbose_name="Краткое описание")
    full_text = tinymce_model.HTMLField(blank=True, verbose_name="Полное описание")
    price = models.DecimalField(max_digits=5, decimal_places=2, verbose_name="Цена", blank=True, null=True)
    published = models.BooleanField(verbose_name=u"Опубликован")
    ordering = models.IntegerField(verbose_name=u"Порядок сортировки", default=0, blank=True, null=True)
    def __unicode__(self):
        return self.name
    
    def pic(self):
        if self.image:
            return u'<img src="%s" width="70"/>' % self.image.url
        else:
            return '(none)'
    pic.short_description = u'Изображение'
    pic.allow_tags = True
    
    class Meta:
        verbose_name_plural = "Товары"
        verbose_name = "Товар"
        
class ProduktImages(models.Model):
    produkt =  models.ForeignKey(Produkt, null=True, blank=True)
    image = models.ImageField(upload_to=make_upload_path, default="",  blank=True,  verbose_name="Изображение")
    def __unicode__(self):
        return self.image
    
    def pic(self):
        if self.image:
            return u'<img src="%s" width="70"/>' % self.image.url
        else:
            return '(none)'
    pic.short_description = u'Изображение'
    pic.allow_tags = True
    
    class Meta:
        verbose_name_plural = "Изображения"
        verbose_name = "Изображение"
    
    
    
    
    
    
    
    
    
    
    
    
    


